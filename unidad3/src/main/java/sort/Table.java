package sort;

public class Table{

    public static void viewArray(int[] a) {
        System.out.print("\n[ ");
        for (int j : a) {
            System.out.print(" " + j);
        }
        System.out.print(" ]\n");
    }

    static long sortArray(int[] a, int method){
        long tInit, tEnd;
        Ordenamiento o = new Ordenamiento();

        tInit = System.currentTimeMillis();
        switch (method){
            case 0:
                o.bubbleSort(a);
                //viewArray(a);
                break;
            case 1:
                o.insertionSort(a);
                //viewArray(a);
                break;
            case 2:
                o.mergeSort(a);
                //viewArray(a);
                break;
            case 3:
                o.quickSort(a);
                //viewArray(a);
                break;
        }
        tEnd = System.currentTimeMillis();
        return (tEnd - tInit);
    }

    static void createRandomArray(int [] array, int n){
        for (int index = 0; index < n; index ++) array[index] = (int) (Math.random() * n);
    }

    public static void main(String[] args) {
        long [][] result= new long[8][4];
        int n = 3000; //Input Data

        for (int inputSize = 0; inputSize < 8; inputSize++) {
            int [] array = new int[n];
            createRandomArray(array,n);
            System.out.println("- n: " + n );
            System.out.print("{ ");
            for (int method = 0; method < 4; method++ ){
                result[inputSize][method] = sortArray(array.clone(),method);
                System.out.print(result[inputSize][method] + "\t\t\t\t");
            }
            n *= 2;
            System.out.println("}");
        }
    }
}
