/*
Crear una estructura Pila implementado con array. El tamaño de la pila se lee desde teclado.
La interfaz se encuentra en Interfaces de la Unidad 4 (InterfacePilaArray.java)
Crear una aplicación que permita verificar el funcionamiento de las operaciones de la pila a través de un menú.

@autor Eduardo Gomez - 4659580
*/

package opcional;

import opcional.excepciones.PilaLlenaException;
import opcional.excepciones.PilaVaciaException;

import java.util.Scanner;

public class AppPilaArray {

    public static void main(String[] args) throws PilaLlenaException, PilaVaciaException {
        PilaArray<Integer> pila = new PilaArray<>();
        mostrarMenu(0);
        Scanner entrada = new Scanner(System.in);
        int option = entrada.nextInt();
        while (option!=0){
            switch (option){
                case 1:
                    mostrarMenu(1);
                    insertarEnPila(pila);
                    break;
                case 2:
                    mostrarMenu(2);
                    eliminarDePila(pila);
                    break;
                case 3:
                    mostrarMenu(3);
                    buscarEnPila(pila);
                    break;
                default:
                    System.out.println("Opción no disponible");
                    break;
            }
            mostrarMenu(0);
            option = entrada.nextInt();
        }
    }

    private static void insertarEnPila(PilaArray pila) throws PilaLlenaException {
        Scanner entrada = new Scanner(System.in);
        int opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion){
                case 1:
                    System.out.println("\nCantidad de elementos de la pila:");
                    pila.iniciar(entrada.nextInt());
                    break;
                case 2:
                    System.out.println("\nInsertar un elemento");
                    pila.apilar(entrada.nextInt());
                    break;
                case 3:
                    System.out.println("\nElementos en lista:");
                    pila.iterar();
                    break;
                default:
                    System.out.println("\n Opción no disponible");
                    break;
            }
            mostrarMenu(1);
            opcion = entrada.nextInt();
        }

    }

    private static void eliminarDePila(PilaArray pila) throws PilaVaciaException {
        Scanner entrada = new Scanner(System.in);
        int opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion){
                case 1:
                    System.out.println("\nPrimer elemento eliminado");
                    pila.desapilar();
                    break;
                default:
                    System.out.println("\nOpción no disponible");
                    break;
            }
            mostrarMenu(2);
            opcion = entrada.nextInt();
        }
    }

    private static void buscarEnPila(PilaArray pila) throws PilaVaciaException {
        Scanner entrada = new Scanner(System.in);
        int opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion){
                case 2:
                    System.out.println("\nBuscar en lista");
                    pila.buscar(entrada.nextInt());
                    break;
                case 1:
                    System.out.println("\n Primer elemento: " + pila.consultar());
                    break;
                default:
                    System.out.println("\nOpción no disponible");
                    break;
            }
            mostrarMenu(2);
            opcion = entrada.nextInt();
        }
    }

    private static void mostrarMenu(int option) {
        switch (option){
            case 0:
                System.out.println("\n 1- Insertar en pila ->" +
                        "\n 2- Eliminar elementos" +
                        "\n 3- Consultar/Buscar");
                break;
            case 1:
                System.out.println("\n 1. Seleccionar la cantidad de elementos" +
                        "\n 2. Insertar" +
                        "\n 3. Mostrar pila" +
                        "\n 0. Salir al menú principal");
                break;
            case 2:
                System.out.println("\n 1. Eliminar el primer elemento" +
                        "\n 0. Salir al menú principal");
                break;
            case 3:
                System.out.println("\n 1. Consultar el primer elemento" +
                        "\n 2. Buscar un elemento");
                break;
        }
    }
}

/* @autor Eduardo Gomez - 4659580 */