package opcional.excepciones;

public class PilaLlenaException extends Throwable {
    public PilaLlenaException() {
        super("Pila llena");
    }
}
/*
@autor Eduardo Gomez
*/