package opcional;
import opcional.excepciones.*;

public interface InterfacePilaArray<T> {
    //crea la pila
    public void iniciar(int cantElementos);

    //insertar el elemento
    public boolean apilar(T elemento) throws PilaLlenaException;

    //quitar el elemento de la cima
    public T desapilar() throws PilaVaciaException;;

    //devuelve el elemento en la cima de la pila
    public T consultar() throws PilaVaciaException;;

    //devuelve la posicion del dato en la pila, siendo 1 el dato que esta
    //en el tope de la pila y 0 si no encuentra
    public int buscar(T dato);

    //devuelve true si estaVac?a la estructura
    public boolean estaVacia();

    //devuelve true si estaLlena la estructura
    public boolean estaLlena();

    //devuelve la cantidad de elementos cargados en la pila
    public int getLongitud();
}

/* @autor Eduardo Gomez - 4659580 */