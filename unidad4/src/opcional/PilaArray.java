package opcional;

import opcional.excepciones.PilaLlenaException;
import opcional.excepciones.PilaVaciaException;

public class PilaArray<T> implements InterfacePilaArray<T>{
    T[] pila = (T[]) new Object[2];
    private int size;

    @Override
    public void iniciar(int cantElementos) {
        pila = (T[]) new Object[cantElementos];
        size = 0;
    }

    @Override
    public boolean apilar(T elemento) throws PilaLlenaException {
        if (estaLlena()) throw new PilaLlenaException();
        else {
            this.size++;
            for (int i = getLongitud()-1; i > 0; i--) pila[i] = pila[i - 1];
            pila[0] = elemento;
            return true;
        }
    }

    @Override
    public T desapilar() throws PilaVaciaException {
        if (estaVacia()) throw new PilaVaciaException();
        else {
            T temp = pila[0];
            for (int i = 0; i < getLongitud(); i++) pila[i] = pila[i + 1];
            this.size--;
            return temp;
        }
    }

    @Override
    public T consultar() throws PilaVaciaException {
        if (estaVacia()) throw new PilaVaciaException();
        else return pila[0];
    }

    @Override
    public int buscar(T dato) {
        for (int i = 0; i < getLongitud(); i++) if (dato.toString().equals(pila[i].toString())) return i + 1;
        return 0;
    }

    @Override
    public boolean estaVacia() {
        return this.size == 0;
    }

    @Override
    public boolean estaLlena() {
        return pila.length == this.size;
    }

    @Override
    public int getLongitud() {
        return this.size;
    }

    public void iterar(){
        for (T element: pila) System.out.println(element);
    }
}

/* @autor Eduardo Gomez - 4659580 */