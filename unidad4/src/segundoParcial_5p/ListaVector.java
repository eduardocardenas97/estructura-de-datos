package segundoParcial_5p;

import segundoParcial_5p.excepciones.ListaLlenaException;
import segundoParcial_5p.excepciones.ListaVaciaException;
import segundoParcial_5p.excepciones.PosicionFueraRangoException;

public class ListaVector<T> implements InterfaceListaVector<T>{
    private int size;
    T[] list = (T[]) new Object[5];

    @Override
    public void iniciar(int cant) {
        list = (T[]) new Object[cant];
        list[0] = null;
        size = 0;
    }

    @Override
    public void insertar(T dato, int pos) throws PosicionFueraRangoException, ListaLlenaException {
        if (pos > tamano()+1 || pos <= 0) throw new PosicionFueraRangoException();
        if(!estaLlena()) {
            size++;
            for (int i = tamano()-1; i >= pos;i--) list[i] = list[i-1];
            list[pos-1] = dato;
        }else throw new ListaLlenaException();
    }

    @Override
    public void insertar(T dato) throws ListaLlenaException {
        if(!estaLlena()) {
            list[size] = dato;
            size++;
        }else throw new ListaLlenaException();
    }

    @Override
    public T suprimir(int pos) throws PosicionFueraRangoException, ListaVaciaException {
        if (pos > tamano() || pos <= 0) throw new PosicionFueraRangoException();
        if(!estaVacia()) {
            for (int i = pos; i < tamano();i++) list[i-1] = list[i];
            size--;
        }else throw new ListaVaciaException();
        return null;
    }

    @Override
    public T suprimir() throws ListaVaciaException {
        if (!estaVacia()) {
            T tmp = list[0];
            for (int i = 1; i < tamano();i++) list[i-1] = list[i];
            size--;
            return tmp;
        }else throw new ListaVaciaException();
    }

    @Override
    public T consultar(int pos) throws ListaVaciaException, PosicionFueraRangoException {
        if (estaVacia()) throw new ListaVaciaException();
        if (pos > tamano() || pos <= 0) throw new PosicionFueraRangoException();
        return list[pos-1];
    }

    @Override
    public boolean estaVacia() { return list.length - size == list.length; }

    @Override
    public boolean estaLlena() { return tamano() == list.length; }

    @Override
    public int buscar(T dato) {
        for (int i = 0; i < tamano(); i++) if (list[i].toString().equals(dato.toString())) return i + 1;
        return 0;
    }

    @Override
    public int tamano() { return size; }

    @Override
    public Object[] ordenar() {
        return null;
    }

    public void iterar(){
        for (int i = 0; i < tamano(); i++) {
            System.out.println(list[i]);
        }
    }
}
/*
    @autor Eduardo Gomez - 4659580
*/