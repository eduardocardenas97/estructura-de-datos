package segundoParcial_5p.excepciones;

public class PosicionFueraRangoException extends Throwable {
    public PosicionFueraRangoException() { super("Posición fuera de rango"); }
}
/*
@autor Eduardo Gomez
*/