/*
Implementar una lista utilizando un vector de objetos (array). La lista debe implementar la InterfaceListaVector.java que se encuentra en Interfaces de la Unidad 4. El método ordenar es opcional implementar (si no va implementar retorne null).

Crear una aplicación que pruebe los diferentes métodos implementados a través de un menú.

La actividad será evaluada por el método de calificación simple, en una escala de 0 a 100, con las siguientes consideraciones:

a. Todas las clases del proyecto entregado compilan sin errores 25%
b. Codifica correctamente la estructura Lista 50%
c. Crea la aplicación para probar los métodos de la Lista mediante menú 25%
*

Tarea sobre Listas Estáticas - 5p Segundo Examen Parcial
@autor Eduardo Gomez - 4659580
*/


package segundoParcial_5p;
import java.util.Scanner;
import segundoParcial_5p.excepciones.ListaLlenaException;
import segundoParcial_5p.excepciones.ListaVaciaException;
import segundoParcial_5p.excepciones.PosicionFueraRangoException;

public class AppListaVector {
    static private void mostrarOpciones(short opcion){
        switch (opcion){
            case 1:
                System.out.println(" 1- Cargar lista" +
                        "\n 2- Eliminar elementos" +
                        "\n 3- Consultar elementos en la lista" +
                        "\n 4- Mostrar lista" +
                        "\n 0- Salir");
                break;
            case 2:
                System.out.println(" 1. Iniciar la lista -> " +
                        "\n 2. Agregar un dato" +
                        "\n 3. Agregar un dato en posición indicada" +
                        "\n 4. Mostrar lista" +
                        "\n 0. Salir al menu principal");
                break;
            case 3:
                System.out.println(" 1. Eliminar primer elemento" +
                        "\n 2. Eliminar elemento dada una posición" +
                        "\n 3. Mostrar lista" +
                        "\n 0. Salir al menu principal");
                break;
            case 4:
                System.out.println(" 1. Consultar elemento dada una posición: " +
                        "\n 2. Buscar un elemento en la lista" +
                        "\n 3. Mostrar lista" +
                        "\n 0. Salir al menu principal");
                break;
        }

    }

    static private void cargarElementos(ListaVector lista) throws ListaLlenaException, PosicionFueraRangoException {
        int opcion;
        Scanner entrada = new Scanner(System.in);
        mostrarOpciones((short)2);
        opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion) {
                case 0:
                    System.out.println("\n\t Saliendo al menu principal");
                    break;
                case 1:
                    System.out.print("\n\t Definir dimensión de la lista: ");
                    lista.iniciar(entrada.nextInt());
                    break;
                case 2:
                    System.out.print("\n\t Agregar un dato: ");
                    lista.insertar(entrada.nextInt());
                    break;
                case 3:
                    System.out.print("\n\t Agregar dato/posición: ");
                    int dato = entrada.nextInt();
                    int pos = entrada.nextInt();
                    lista.insertar(dato, pos);
                    break;
                case 4:
                    System.out.println("\n\t Elementos en lista");
                    lista.iterar();
                    System.out.println("\n");
                    break;
                default:
                    System.out.println("\n\t Posición incorrecta");
                    break;
            }
            mostrarOpciones((short)2);
            opcion = entrada.nextInt();
        }
    }

    static private void eliminarElementos(ListaVector lista) throws ListaVaciaException, PosicionFueraRangoException {
        int opcion;
        Scanner entrada = new Scanner(System.in);
        mostrarOpciones((short)3);
        opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion){
                case 0:
                    System.out.println("\n\t Saliendo al menu principal");
                    break;
                case 1:
                    System.out.println("\n\t Eliminando primer elemento");
                    lista.suprimir();
                    break;
                case 2:
                    System.out.print("\n\t Posición a eliminar: ");
                    lista.suprimir(entrada.nextInt());
                    break;
                case 3:
                    System.out.println("\n\t Elementos en lista");
                    lista.iterar();
                    System.out.println("\n");
                    break;
                default:
                    System.out.println("\n\t Opción no valida");
                    break;
            }
            mostrarOpciones((short)3);
            opcion = entrada.nextInt();
        }
    }

    private static void consultarElementos(ListaVector lista) throws ListaVaciaException, PosicionFueraRangoException {
        int opcion;
        Scanner entrada = new Scanner(System.in);
        mostrarOpciones((short)4);
        opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion){
                case 0:
                    System.out.println("\t Saliendo al menu principal");
                    break;
                case 1:
                    System.out.println("\tElemento en posición ingresada: "+ lista.consultar(entrada.nextInt()));
                    break;
                case 2:
                    System.out.println("\tEl dato se encuentra en la posición: " +lista.buscar(entrada.nextInt()));
                    break;
                case 3:
                    System.out.println("\n Elementos en lista");
                    lista.iterar();
                    System.out.println("\n");
                    break;
                default:
                    System.out.println("Opción invalida");
                    break;
            }
            mostrarOpciones((short)4);
            opcion = entrada.nextInt();
        }
    }

    public static void main(String[] args) throws ListaLlenaException, PosicionFueraRangoException, ListaVaciaException {
        ListaVector<Integer> lista = new ListaVector<>();
        int opcion;
        Scanner entrada = new Scanner(System.in);
        mostrarOpciones((short)1);
        opcion = entrada.nextInt();
        while (opcion != 0){
            switch (opcion) {
                case 1:
                    System.out.println("\n---- Cargando ----");
                    cargarElementos(lista);
                    lista.iterar();
                    break;
                case 2:
                    System.out.println("\n---- Eliminando ----");
                    eliminarElementos(lista);
                    break;
                case 3:
                    System.out.println("\n---- Consultando elementos ----");
                    consultarElementos(lista);
                    break;
                case 4:
                    System.out.println("\n---- Mostrando elementos ----");
                    lista.iterar();
                    break;
                default:
                    System.out.println("\nPosición incorrecta");
                    break;
            }
            mostrarOpciones((short)1);
            opcion = entrada.nextInt();
        }
    }

}

/*
    @autor Eduardo Gomez - 4659580
*/