/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_1_NB;

public class ejercicio3 {
    static void iterarLista(ListaEnlazadaSimple<Integer> lista){
        IteradorLista<Integer> it = lista.getIterador();
        while (it.hasNext()) {
            System.out.println("Valor: " + it.next());
        }
    }

    static ListaEnlazadaSimple<Integer> generarInterseccion(ListaEnlazadaSimple<Integer> lista1, ListaEnlazadaSimple<Integer> lista2){
        ListaEnlazadaSimple<Integer> lista3 = new ListaEnlazadaSimple<>();
        IteradorLista<Integer> it1 = lista1.getIterador();
        IteradorLista<Integer> it2 = lista2.getIterador();

        while (it1.hasNext()) {
            int eList1 = it1.next(); //Dato del nodo (lista 1)
            while (it2.hasNext()) {
                int eList2 = it2.next(); //Dato del nodo (lista 2)
                if (eList1 == eList2) lista3.insertar( eList1 );
            }
            it2.first();
        }
        return lista3;
    }

    public static void main(String[] args) {
        try {
            ListaEnlazadaSimple<Integer> lista1 = new ListaEnlazadaSimple<>();
            ListaEnlazadaSimple<Integer> lista2 = new ListaEnlazadaSimple<>();

            for (int i = 1; i < 7; i++)  lista1.insertar(i*10);
            for (int i = 5; i > 1; i--)  lista2.insertar(i*20);
            System.out.println("Lista 1: ");
            iterarLista(lista1);
            System.out.println("\nLista 2: ");
            iterarLista(lista2);
            System.out.println("\nValores de intersección: ");
            iterarLista( generarInterseccion(lista1,lista2) );


        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }
    }
}
