/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_1_NB;

public class ejercicio2 {
    public static void main(String[] args) {
        try {
            ListaEnlazadaSimple<Integer> lista = new ListaEnlazadaSimple<>();
            for (int i = 1; i < 5; i++)  lista.insertar(i*10);
            IteradorLista<Integer> it = lista.getIterador();
            while (it.hasNext()) {
                System.out.println("Valor: " + it.next());
            }
        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }
    }
}
