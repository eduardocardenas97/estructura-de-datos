/*
* @autor Eduardo Gomez
* CI: 4659580
*/

package actividad4_1_NB;
public class ejercicio1 {
    static void iterarLista(ListaEnlazadaSimple<Integer> lista) throws Exception {
        for (int i = 1; i <= lista.getTamanoLista(); i++) {
            System.out.println("Valor " + i + ": " + lista.obtenerDato(i));
        }
    }

    public static void main(String[] args) {
        ListaEnlazadaSimple<Integer> lista = new ListaEnlazadaSimple<>();
        try {
            lista.insertar(4);
            lista.insertar(5);
            lista.insertar(6);
            lista.insertar(11);
            lista.insertar(32);
            iterarLista(lista);
            System.out.println("\nSe elimina la posicion 2");
            lista.eliminar(2);
            iterarLista(lista);

            //Se busca un dato
            System.out.println("\nSe busca un dato '11' ");
            System.out.println("Posición en lista: " + lista.buscar(11) + "\n");

            //Insertamos un nuevo dato
            System.out.println("Se agrega un nuevo elemento a la posición '1'");
            lista.insertar(992,1);
            iterarLista(lista);

        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }

    }
}
