package actividad4_1_NB;

public interface InterfazIterador<TipoDeDato> {

    public void first();

    /**
     * Metodo que indica con TRUE o FALSE si es que aun existen datos en la coleccion
     * que no fueron visitados por el iterador. Cuando llega al final de la coleccion, retorna FALSE
     * @return TRUE o FALSE
     */
    public boolean hasNext();

    /**
     * Metodo que retorna el siguiente dato almacenado en la coleccion, siempre y cuando
     * aun existan datos pendientes de ser visitados.
     * @return Dato almacenado en la coleccion
     */
    public TipoDeDato next();
}
