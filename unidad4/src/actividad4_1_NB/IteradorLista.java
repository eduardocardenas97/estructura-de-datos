/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_1_NB;

public class IteradorLista<TipoDeDato> implements InterfazIterador<TipoDeDato> {

    private NodoLista<TipoDeDato> nodoCabecera;

    private NodoLista<TipoDeDato> nodoActual;

    private int posActual;

    /**
     * Constructor de la clase.
     * @param cabecera Referencia a la cabecera de la lista sobre la que se iterara
     */
    public IteradorLista(NodoLista<TipoDeDato> cabecera) {
        nodoCabecera = cabecera;
        nodoActual = cabecera;
        posActual = 0;
    }

    @Override
    public void first() {
        nodoActual = nodoCabecera;
        posActual = 0;
    }

    @Override
    public boolean hasNext() {
        return nodoActual.getSiguiente() != null;
    }

    @Override
    public TipoDeDato next() {
        posActual ++;
        nodoActual = nodoActual.getSiguiente();
        return nodoActual != null ? nodoActual.getDato() : null;
    }
}
