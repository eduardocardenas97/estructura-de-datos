/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_1_NB;

public class ListaEnlazadaSimple<TipoDeDato> implements InterfazLista<TipoDeDato> {

    private final NodoLista<TipoDeDato> cabecera;
    private int tamanoLista;
    public ListaEnlazadaSimple() {
        tamanoLista = 0;
        cabecera = new NodoLista<>();
        cabecera.setSiguiente( null );
    }

    @Override
    public void insertar(TipoDeDato nuevoDato) {
        NodoLista<TipoDeDato> nuevoNodo = new NodoLista<>();
        nuevoNodo.setDato( nuevoDato );
        nuevoNodo.setSiguiente( cabecera.getSiguiente() );
        cabecera.setSiguiente( nuevoNodo );
        tamanoLista ++;
    }

    @Override
    public void insertar(TipoDeDato nuevoDato, int posicion) throws Exception {
        int posActual = 1;
        NodoLista<TipoDeDato> actual;
        NodoLista<TipoDeDato> nuevo = new NodoLista<>();
        nuevo.setDato( nuevoDato );

        if ( posicion == 1 ) {
            nuevo.setSiguiente( cabecera.getSiguiente() );
            cabecera.setSiguiente( nuevo );
        } else if (posicion > 1 && posicion <= tamanoLista) {
            actual = cabecera.getSiguiente();
            while (actual != null) {
                if (posicion == posActual+1) {
                    nuevo.setSiguiente( actual.getSiguiente() );
                    actual.setSiguiente( nuevo );
                    tamanoLista ++;
                    break;
                } else {
                    actual = actual.getSiguiente();
                    posActual++;
                }
            }
        } else {
            throw new Exception("Intento de acceso a posicion inexistente");
        }

    }

    @Override
    public int buscar(TipoDeDato dato) {
        int posActual = 1;
        NodoLista actual;
        actual = cabecera.getSiguiente();
        while (actual != null) {
            if (actual.getDato() == dato) return posActual;
            actual = actual.getSiguiente();
            posActual++;
        }
        return 0;
    }

    @Override
    public TipoDeDato obtenerDato(int posicion) throws Exception {
        int posActual = 1;
        TipoDeDato retorno = null;
        NodoLista<TipoDeDato> actual;

        if (posicion > 0 && posicion <= tamanoLista) {
            actual = cabecera.getSiguiente();
            while (actual != null) {
                if (posicion == posActual) {
                    retorno = actual.getDato();
                    break;
                } else {
                    actual = actual.getSiguiente();
                    posActual++;
                }
            }
        } else {
            throw new Exception("Intento de acceso a posicion inexistente");
        }

        return retorno;
    }

    @Override
    public void eliminar() {
        if (!esVacia()) {
            cabecera.siguiente = cabecera.siguiente.siguiente;
            tamanoLista --;
        }
    }

    @Override
    public void eliminar(int posicion) throws Exception {
        int posActual = 1;
        NodoLista actual;

        if (posicion == 1) eliminar();
        else if (posicion > 1 && posicion <= tamanoLista) {
            actual = cabecera.getSiguiente();
            while (actual != null) {
                if (posicion == posActual+1) {
                    actual.siguiente = actual.siguiente.siguiente;
                    tamanoLista --;
                    break;
                } else {
                    actual = actual.getSiguiente();
                    posActual++;
                }
            }
        } else {
            throw new Exception("Intento de acceso a posicion inexistente");
        }

    }

    @Override
    public int getTamanoLista() {
        return tamanoLista;
    }

    @Override
    public boolean esVacia() {
        return tamanoLista == 0;
    }
    
    public IteradorLista<TipoDeDato> getIterador() {
        return new IteradorLista<>(cabecera);
    }
}
