/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.colas;

import java.text.SimpleDateFormat;
import java.util.Date;

public class MensajeLog {
    private long timestamp;
    private String lineaLog;

    public MensajeLog(long timestamp, String lineaLog) {
        this.timestamp = timestamp;
        this.lineaLog = lineaLog;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public String getLineaLog() {
        return lineaLog;
    }

    public void setLineaLog(String lineaLog) {
        this.lineaLog = lineaLog;
    }

    static String formatTime(long val){
        Date date=new Date(val);
        SimpleDateFormat df2 = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        String dateText = df2.format(date);
        return dateText;
    }

    @Override
    public String toString() {
        return "" + formatTime(timestamp) + "|" + lineaLog + "\n";
    }
}
