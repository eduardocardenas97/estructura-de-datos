/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.colas;

public class PruebaManejoLogs {
    public static void main(String[] args) {
        Cola<MensajeLog> cola = new Cola<>();
        ManejoLogs manejoLogs = new ManejoLogs();
        try {
            cola.encolar( manejoLogs.cargarLog("Mensaje de log 1") );
            cola.encolar( manejoLogs.cargarLog("Mensaje de log 2") );
            cola.encolar( manejoLogs.cargarLog("Mensaje de log 3") );
            manejoLogs.descargarLogs(cola);
        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }
    }
}
