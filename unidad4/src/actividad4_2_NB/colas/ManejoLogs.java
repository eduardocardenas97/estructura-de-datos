/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.colas;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class ManejoLogs {

    public MensajeLog cargarLog(String msg){
        MensajeLog nuevoLog = new MensajeLog(System.currentTimeMillis(),msg);
        return nuevoLog;
    }

    static void escribirEnArchivo(MensajeLog nodo){
        BufferedWriter bw = null;
        FileWriter fw = null;

        try {
            String data = nodo.toString();
            File file = new File("./src/actividad4_2_NB/colas/archivoManejoLog.txt");

            if (!file.exists()) {
                file.createNewFile();
            }

            fw = new FileWriter(file.getAbsoluteFile(), true);
            bw = new BufferedWriter(fw);
            bw.write(data);
            System.out.println("información agregada!");

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (bw != null)  bw.close();
                if (fw != null)  fw.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void descargarLogs(Cola<MensajeLog> cola){
        int n = cola.tamanoCola();
        for (int i=1; i<=n; i++) {
            escribirEnArchivo(cola.desencolar());
        }
    }
}
