/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.pilas;

public class Pila<TipoDeDato> implements InterfazPila<TipoDeDato> {

    private NodoPila<TipoDeDato> cabecera;
    private int tamanoPila;

    public Pila() {
        tamanoPila = 0;
        cabecera = new NodoPila<>();
        cabecera.siguiente = null;
    }
    
    @Override
    public void apilar(TipoDeDato nuevoDato){
    	NodoPila<TipoDeDato> nuevoNodo = new NodoPila<>();
        nuevoNodo.dato = nuevoDato;
        nuevoNodo.siguiente = cabecera.siguiente;
        cabecera.siguiente = nuevoNodo;
        tamanoPila = tamanoPila + 1;
    }
    
    @Override
    //Si se intenta desapilar algo de una lista vacia, retorna null
    public TipoDeDato desapilar(){
    	TipoDeDato ret = null;
    	if (!esVacia()) {
    	    ret = cabecera.siguiente.dato;
            cabecera.siguiente = cabecera.siguiente.siguiente;
            tamanoPila = tamanoPila - 1;
        }
        return ret;
    }
    
    @Override
    //Si se intenta obtener algo de una lista vacia, retorna null
    public TipoDeDato obtenerTope(){
    	return esVacia() ? null : cabecera.getSiguiente().getDato();
    }
    
    @Override
    public void anular(){
    	cabecera.siguiente = null;
    	tamanoPila = 0;
    }
    
    @Override
    public int tamanoPila() {
        return tamanoPila;
    }

    @Override
    public boolean esVacia() {
        return tamanoPila == 0;
    }

    public void reemplazar(TipoDeDato valor, TipoDeDato reemplazo){
        NodoPila<TipoDeDato> nodoActual = new NodoPila<>();
        nodoActual = cabecera;
        
        while (nodoActual.getSiguiente() != null){
            if ( valor == nodoActual.getDato() ) nodoActual.setDato(reemplazo);
            nodoActual = nodoActual.getSiguiente();
        }
    }
        
}
