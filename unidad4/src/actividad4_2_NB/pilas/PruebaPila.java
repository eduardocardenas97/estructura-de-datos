/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.pilas;

public class PruebaPila {

    public static void main(String[] args) {

        Pila<Integer> pila = new Pila<>();
        Pila<String> pilaStr = new Pila<>();
        
        try {
            pila.apilar(1);
            pila.apilar(2);
            pila.apilar(3);

            int n = pila.tamanoPila();
            for (int i = 1; i <= n; i++) {
                System.out.println("Valor " + i + ": " + pila.desapilar());
            }

            //otra forma de iterar..
            pilaStr.apilar("a");
            pilaStr.apilar("b");
            pilaStr.apilar("c");
            while (!pilaStr.esVacia()) {
                System.out.println("Valor : " + pilaStr.desapilar());
            }
        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }

    }
}
