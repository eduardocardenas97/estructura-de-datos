/*
 * @autor Eduardo Gomez
 * CI: 4659580
 */

package actividad4_2_NB.pilas;

public class PruebaPilaReemplazar {
    public static void main(String[] args) {
        Pila<Integer> pila = new Pila<>();
        Pila<String> pilaStr = new Pila<>();

        try {
            pila.apilar(1);
            pila.apilar(2);
            pila.apilar(3);

            while (!pila.esVacia()) {
                System.out.println("Valor : " + pila.desapilar());
            }


            pilaStr.apilar("aa");
            pilaStr.apilar("b");
            pilaStr.apilar("b");
            pilaStr.reemplazar("b","pepe");
            pilaStr.apilar("c");

            while (!pilaStr.esVacia()) {
                System.out.println("Valor : " + pilaStr.desapilar());
            }

        } catch (Exception e) {
            System.out.println("Error al ejecutar el programa. " + e.getMessage());
        }
    }
}
