package ejercicio7;

public class Nodo <Alumno> implements Comparable{
    Nodo sig;
    Alumno dato;
    public Nodo(){

    }
    public Nodo(Alumno dato,Nodo sig) {
        this.sig = sig;
        this.dato = dato;
    }

    public Nodo getSig() {
        return sig;
    }

    public void setSig(Nodo sig) {
        this.sig = sig;
    }

    public Alumno getDato() {
        return dato;
    }

    public void setDato(Alumno dato) {
        this.dato = dato;
    }

    @Override
    public String toString() {
        return "Nodo{" +
                "sig=" + sig +
                ", dato=" + dato +
                '}';
    }


    @Override
    public int compareTo(Object o) {
        return 0;
    }
}
/*
@autor Eduardo Gomez
*/