/*
Ejercicio 7 - TP3

a) Crear una clase ListaEnlazadaOrdenada.java que implemente la interface InterfaceListaOrdenada.java
que se encuentra en Educa. La implementación no debe utilizar nodo cabecera (nodo ficticio).
b) Crear una clase Alumno.java con las siguientes propiedades privadas int cedula, String nombre, String
apellido, int edad. Agregar los métodos constructores, set, get y toString necesarios.
c) Crear una aplicación (AppListaEOrdenada.java) que permita, a través de un menú: 1) leer el archivo
Alumnos.txt (que se encuentra en Educa/Recursos) y cargar en la lista a través de la clase Alumno, 2)
Imprimir la lista, 3) Mostrar el total de alumnos, 4) Mostrar el promedio de edad de los alumnos, 5) Insertar
un alumno por teclado, 6) Eliminar un alumno (de cualquiera de las posiciones posibles, usar un submenú),
7) Buscar un alumno e indicar si está o no en la lista, 8) Consultar el alumno que esté en la posición
ingresada por teclado, y 0) Finalizar.
El formato del archivo no se puede modificar. La aplicación debe leer el contenido del archivo
adecuadamente. Ver la interface Comparable.java que tiene un método compareTo (tal vez sea necesario
implementar este método).

@autor Eduardo Gomez - 4659580
*/

package ejercicio7;
import java.util.Scanner;
import ejercicio7.excepciones.ListaVaciaException;
import ejercicio7.excepciones.PosicionFueraRangoException;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AppListaOrdenada {

    public static void cargarAlumno(ListaOrdenada listaOrdenada) throws FileNotFoundException, IOException {
        String cadena;
        listaOrdenada.iniciar();
        FileReader f = new FileReader("./src/ejercicio7/alumnos.txt");
        BufferedReader b = new BufferedReader(f);
        while((cadena = b.readLine())!=null) {
            String[] tokens = cadena.split(", ");
            Alumno alumno = new Alumno(
                    Integer.parseInt(tokens[0]),
                    Integer.parseInt(tokens[3]),
                    tokens[1],
                    tokens[2]
            );
            listaOrdenada.insertar(alumno);
        }
        listaOrdenada.iterar();
        b.close();
    }

    public static void mostrarAlumnos(ListaOrdenada listaOrdenada){
        listaOrdenada.nodo = listaOrdenada.nodoInicio;
        for (int i = 1; i <= listaOrdenada.size(); i++) {
            System.out.println(i +" "+ listaOrdenada.nodo.getDato());
            listaOrdenada.next();
        }
    }

    public static int mostrarTotalAlumnos(ListaOrdenada listaOrdenada){
        return listaOrdenada.size();
    }

    public static int mostrarPromedioEdad(ListaOrdenada listaOrdenada){
        int sum = 0;
        listaOrdenada.nodo = listaOrdenada.nodoInicio;
        while (listaOrdenada.nodo != null){
            Alumno alu = (Alumno) listaOrdenada.nodo.getDato();
            sum += alu.getEdad();
            listaOrdenada.next();
        }
        return sum / listaOrdenada.getTamanoLista();
    }

    public static void insertarAlumno(ListaOrdenada listaOrdenada){
        Scanner entrada = new Scanner(System.in);
        String nombre, apellido;
        int cedula, edad;
        cedula = entrada.nextInt();
        nombre = entrada.next();
        apellido = entrada.next();
        edad = entrada.nextInt();
        listaOrdenada.insertar(new Alumno(cedula,edad,nombre,apellido));
    }

    public static void eliminarAlumno(ListaOrdenada listaOrdenada){
        System.out.println("\nSeleccionar alumno a eliminar: ");
        mostrarAlumnos(listaOrdenada);
        int posicion;
        Scanner input = new Scanner(System.in);
        posicion = input.nextInt();
        try {
            System.out.println("Eliminado: "+ listaOrdenada.remover(posicion));
        } catch (ListaVaciaException e) {
            e.printStackTrace();
        } catch (PosicionFueraRangoException e) {
            e.printStackTrace();
        }
    }

    public static void buscarAlumno(ListaOrdenada listaOrdenada){
        Scanner entrada = new Scanner(System.in);
        String nombre, apellido;
        int cedula, edad;
        cedula = entrada.nextInt();
        nombre = entrada.nextLine();
        apellido = entrada.nextLine();
        edad = entrada.nextInt();
        Alumno alu = new Alumno(cedula,edad,nombre,apellido);
        if (listaOrdenada.buscar(alu) != 0) System.out.println("El alumno se encuentra en la lista");
    }

    public static void consultarAlumno(ListaOrdenada listaOrdenada){
        System.out.println("\nIngrese posición: ");
        mostrarAlumnos(listaOrdenada);
        int posicion;
        Scanner input = new Scanner(System.in);
        posicion = input.nextInt();
        try {
            System.out.println("El alumno que se encuentra en la posicion: " + listaOrdenada.consultar(posicion));
        } catch (PosicionFueraRangoException e) {
            e.printStackTrace();
        }
    }

    public static void main(String[] args) throws IOException {
        ListaOrdenada <Alumno> listaOrdenada = new ListaOrdenada<>();
        Scanner entrada = new Scanner(System.in);
        int option = entrada.nextInt();
        while (option != 0) {
            switch (option) {
                case 0:
                    System.exit(0);
                    break;
                case 1:
                    cargarAlumno(listaOrdenada);
                    break;
                case 2:
                    System.out.println("\n Lista de alumnos:");
                    mostrarAlumnos(listaOrdenada);
                    break;
                case 3:
                    System.out.println("\n Total de alumnos" + mostrarTotalAlumnos(listaOrdenada));
                    break;
                case 4:
                    System.out.println("\nPromedio de edades: " + mostrarPromedioEdad(listaOrdenada));
                    break;
                case 5:
                    System.out.println("\nInsertar un alumno: (cedula,nombre,apellido,edad)");
                    insertarAlumno(listaOrdenada);
                    break;
                case 6:
                    eliminarAlumno(listaOrdenada);
                    break;
                case 7:
                    buscarAlumno(listaOrdenada);
                    break;
                case 8:
                    consultarAlumno(listaOrdenada);
                    break;
            }
            option = entrada.nextInt();
        }
    }
}
/*
@autor Eduardo Gomez
*/
