
package ejercicio7;

import ejercicio7.excepciones.*;

/**
 * Interface que deben implementar las estructuras que se crean usando listas
 * enlazadas ordenadas. Todos los m�todos de una interface son abstractos.
 */
public interface InterfaceLista<T> {
       /**
        * inicia la estructura
        */ 
       public void iniciar();
      
       /**
        * inserta un elemento en el lugar adecuado (de acuerdo al orden)
        * @param dato - el elemento que se desea insertar en la lista
        */
       public void insertar(T dato);

       /**
        * Remueve un elemento del frente o lanza una excepci�n si la lista 
		* esta vacia
        * @return el objeto eliminado
        * @throws ListaVaciaException
        */
       public T remover() throws ListaVaciaException;

       /**
        * remueve un elemento del final o lanza una excepci�n si la lista 
		* esta vacia
        * @return el objeto eliminado
        * @throws ListaVaciaException si la lista esta vac�a
        */
       public T removerAtras() throws ListaVaciaException;

       /**
        * Remueve el primer elemento (de la lista) que coincida con el dato 
        * recibido como par�metro o lanza una excepci�n si la lista esta vacia
        * o no encuentra el dato a remover.
        * @throws ListaVaciaException si la lista esta vac�a
        * @throws NoSuchElementException si no encuentra el dato a eliminar
        */
       public void remover(T dato) throws ListaVaciaException, 
                                          NoSuchElementException;

       /**
        * Remueve un elemento de la posicion pos o lanza una excepcion si
        * pos esta fuera de rango (pos < 1 || pos > size())
        * @return el objeto eliminado
        * @throws ListaVaciaException si la lista esta vac�a
        * @throws PosicionFueraRangoException si no encuentra la pos a eliminar
       */
       public T remover(int pos) throws ListaVaciaException, 
                                        PosicionFueraRangoException;

       /**
        * busca un elemento y devuelve su ubicaci�n en la lista
        * @return 0 si no existe (o la lista esta vacia), o la posicion dentro 
        * de la lista si existe, siendo 1 el primer elemento
        */
       public int buscar(T dato);

       /**
        * Retorna el elemento ubicado en pos o lanza una excepcion si
        * pos esta fuera de rango (pos < 1 || pos > size())
        * @return el elemento en la posicion indicada
        * @throws PosicionFueraRangoException si no encuentra la pos a consultar
        */
       public T consultar(int pos) throws PosicionFueraRangoException;

       /**
        * retorna todos los datos de la lista como un vector de objetos
        * @return un vector de objetos con los elementos de la lista enlazada
        */
       public Object[] toArray();

	   /**
        * Retorna todos los datos de la lista como un vector de objetos T
		* Si la capacidad del array a es menor a la lista, se carga hasta el 
		* limite de a.
        * @return vector "a" con los elementos de la lista enlazada
        */
       public T[] toArray(T[] a);
	   
       /**
       * verifica si la estructura esta o no vac�a
       * @return true si la estructura est� vac�a o false en caso contrario
       */
       public boolean isEmpty();
       
       /**
       * Retorna el numero de elementos en la lista enlazada.
       * @return el tamanho de la lista
       */
       public int size();
}
