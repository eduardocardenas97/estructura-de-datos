package ejercicio7;

import ejercicio7.excepciones.ListaVaciaException;
import ejercicio7.excepciones.NoSuchElementException;
import ejercicio7.excepciones.PosicionFueraRangoException;

public class ListaOrdenada<Alumno> implements InterfaceLista<Alumno>{
    Nodo<Alumno> nodo = new Nodo<>();
    Nodo<Alumno> nodoInicio = new Nodo<>();
    int tamanoLista = 0;

    public int getTamanoLista() {
        return tamanoLista;
    }

    public void iterar(){
        nodo = nodoInicio;
        while (nodo != null){
            System.out.println( nodo.getDato() );
            nodo = nodo.getSig();
        }
    }

    public void next(){
        nodo = nodo.getSig();
    }

    @Override
    public void iniciar() {
        nodo.setSig(null);
    }

    @Override
    public void insertar(Alumno dato) {
        Nodo nuevoNodo = new Nodo(dato,null);
        if (tamanoLista == 0) {
            nodo.setDato(dato);
            nodoInicio = nodo;
            tamanoLista++;
        }else {
            if (nodoInicio.getDato().toString().compareTo(dato.toString()) > 0) {
                nuevoNodo.setSig( nodoInicio );
                nodoInicio = nuevoNodo;
            }else {
                nodo = nodoInicio;
                Nodo nodoAnterior = nodoInicio;
                while (nodo.getDato().toString().compareTo(dato.toString()) <= 0 && nodo.getSig() != null){
                    nodoAnterior = nodo;
                    nodo = nodo.getSig();
                }
                if (nodo.getDato().toString().compareTo(dato.toString()) <= 0) {
                    nodo.setSig( nuevoNodo );
                }else {
                    nuevoNodo.setSig(nodo);
                    nodoAnterior.setSig(nuevoNodo);
                }
            }
            tamanoLista++;
        }
    }

    @Override
    public Alumno remover() throws ListaVaciaException {
        if (isEmpty()){ throw new ListaVaciaException(); }
        Alumno eliminado = nodoInicio.getDato();
        nodoInicio = nodoInicio.getSig();
        tamanoLista--;
        return eliminado;
    }

    @Override
    public Alumno removerAtras() throws ListaVaciaException {
        nodo = nodoInicio;
        if (isEmpty()) throw new ListaVaciaException();
        if (tamanoLista < 2) {
            Alumno tmp = nodo.getDato();
            nodoInicio = nodoInicio.getSig();
            tamanoLista--;
            return tmp;
        }else {
            while (nodo.getSig().getSig() != null){
                nodo = nodo.getSig();
            }
            Alumno tmp = (Alumno) nodo.getSig().getDato();
            nodo.setSig( nodo.getSig().getSig() );
            tamanoLista--;
            return tmp;
        }
    }

    @Override
    public void remover(Alumno dato) throws ListaVaciaException, NoSuchElementException {
        nodo = nodoInicio;
        boolean encontrado = false;
        if (isEmpty()) throw new ListaVaciaException();

        if (dato.toString().equals( nodo.getDato().toString() )) {
            tamanoLista--;
            nodoInicio = nodoInicio.getSig();
        } else {
            while (nodo.getSig() != null) {
                if ( dato.toString().equals( nodo.getSig().getDato().toString() ) ) {
                    nodo.setSig( nodo.getSig().getSig() );
                    tamanoLista--;
                    encontrado = true;
                    break;
                } else nodo = nodo.getSig();
            }
            if(!encontrado){ throw new NoSuchElementException(); }
        }
    }

    @Override
    public Alumno remover(int pos) throws ListaVaciaException, PosicionFueraRangoException {
        if (isEmpty()) throw new ListaVaciaException();
        if ((pos > tamanoLista) && (pos < tamanoLista)) throw new PosicionFueraRangoException();
        nodo = nodoInicio;
        if (pos == 1) {
            Alumno tmp = (Alumno) nodo.getDato();
            nodoInicio = nodoInicio.getSig();
            return tmp;
        }else {
            for (int i = 1; i < pos-1; i++) nodo = nodo.getSig();
            Alumno tmp = (Alumno) nodo.getSig().getDato();
            nodo.setSig( nodo.getSig().getSig() );
            return tmp;
        }
    }

    @Override
    public int buscar(Alumno dato) {
        nodo = nodoInicio;
        for (int i = 1; i <= tamanoLista; i++){
            if (dato.toString().equals( nodo.getDato().toString() ))  return i;
            nodo = nodo.getSig();
        }
        return 0;
    }

    @Override
    public Alumno consultar(int pos) throws PosicionFueraRangoException {
        if ((pos > 0) && (pos < tamanoLista)) {
            nodo = nodoInicio;
            for (int i = 1; i < pos; i++) nodo = nodo.getSig();
            return nodo.getDato();
        }else {
            throw new PosicionFueraRangoException();
        }
    }

    @Override
    public Object[] toArray() {
        Object[] objeto = new Object[tamanoLista];
        nodo = nodoInicio;
        for (int i = 1; i <= tamanoLista; i++) {
            objeto[i-1] = nodo.getDato();
            nodo = nodo.getSig();
        }
        return objeto;
    }

    @Override
    public boolean isEmpty() {
        return nodoInicio == null ? true : false;
    }

    @Override
    public Alumno[] toArray(Alumno[] a) {
        nodo = nodoInicio;
        for (Alumno alumno : a) {
            alumno = nodo.getDato();
            nodo = nodo.getSig();
        }
        return a;
    }

    @Override
    public int size() {
        if (nodoInicio == null) { return 0; }
        Nodo aux = nodoInicio;
        int sizeLista = 1;
        while (aux.getSig() != null){
            aux = aux.getSig();
            sizeLista++;
        }
        return sizeLista;
    }


}
/*
@autor Eduardo Gomez
*/