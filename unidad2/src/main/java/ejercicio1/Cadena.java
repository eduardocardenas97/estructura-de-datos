package ejercicio1;

public class Cadena {
    String str1, str2;
    public Cadena (String str1, String str2){
        this.str1 = str1;
        this.str2 = str2;
    }

    public boolean  esSubstring(){
        int i=0, n, contador=0;
        for (n=0; n<str2.length();n++){
            if (str1.charAt(i) == str2.charAt(n)){
                contador++;
                i++;
                if(contador == str1.length()) return true;
            } else {
              i = 0;
              contador = 0;
            }
        }
        return false;
    }

    @Override
    public String toString() {
        return "Cadena{" +
                "str1='" + str1 + '\'' +
                ", str2='" + str2 + '\'' +
                '}';
    }
}
