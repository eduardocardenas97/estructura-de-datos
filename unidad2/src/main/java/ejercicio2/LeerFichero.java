package ejercicio2;

import java.io.*;
import java.util.Arrays;

public class LeerFichero {

    public static void muestraContenido(String archivo) throws FileNotFoundException, IOException {
        String cadena;
        String rutaAbsoluta = new File(archivo).getAbsolutePath();
        FileReader f = new FileReader(rutaAbsoluta);
        BufferedReader b = new BufferedReader(f);
        try {
            while((cadena = b.readLine())!=null) {
                String[] parts = cadena.split(" ");
                for (int i = 0; i < parts.length; i++) {
                    System.out.println(parts[i]);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        b.close();
    }

    public static void main(String[] args) throws IOException {
        muestraContenido("archivo.txt");
    }

}