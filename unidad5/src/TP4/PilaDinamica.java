package TP4;

import TP4.excepciones.PilaVaciaException;

public class PilaDinamica<T> implements InterfacePilaDinamica<Object> {
    Nodo nodo = new Nodo();
    int tamanoPila;

    @Override
    public void iniciar() {
        nodo.setSiguiente(null);
    }

    @Override
    public void apilar(Object dato) {
         tamanoPila++;
         nodo.setSiguiente(new Nodo(dato, nodo.getSiguiente()));
    }

    @Override
    public Object desapilar() throws PilaVaciaException {
        if (!isEmpty()) {
            tamanoPila--;
            Object tmp = nodo.getSiguiente().getDato();
            nodo.setSiguiente(nodo.getSiguiente().getSiguiente());
            return tmp;
        } else {
            throw new PilaVaciaException();
        }
    }

    @Override
    public Object consultar() throws PilaVaciaException {
        if (isEmpty()) throw new PilaVaciaException();
        return nodo.getDato();
    }

    @Override
    public boolean isEmpty() {
        return size() == 0;
    }

    @Override
    public int buscar(Object dato) {
        return 0;
    }

    @Override
    public int size() {
        return tamanoPila;
    }
}
