/*
* 1. Los datos se almacenan en la memoria de la computadora utilizando una representación binaria. Esto
significa que la representación de los enteros en base 10 que se utiliza en los programas debe convertirse
a la representación en base 2. Uno de los algoritmos que realiza esta conversión consiste en dividir
repetidamente el entero entre 2, siendo los restos de dichas divisiones los dígitos del número en la
representación en base 2. Implemente una estructura que haciendo uso de una pila, convierta un entero
positivo en base 10 a base 2 (leer por teclado) e imprima el resultado en pantalla
* @autor Eduardo Gomez - CI 4659580
* */

package TP4;

import TP4.excepciones.PilaVaciaException;
import java.util.Scanner;

public class AppEjercicio11 {
    public static void main(String[] args) throws PilaVaciaException {
        Scanner entrada = new Scanner(System.in);
        System.out.print("Ingrese un numero: ");
        Conversor numero = new Conversor( entrada.nextInt() );
        numero.convertir();
        System.out.print("\nRepresentación binaria: ");
        numero.mostrar();
    }
}
