package TP4;

import TP4.excepciones.PilaVaciaException;

public class Conversor {
    int numero;
    boolean residuo;

    PilaDinamica<Integer> pila = new PilaDinamica<>();


    public Conversor(int numero) {
        this.numero = numero;
        pila.iniciar();
    }

    public void convertir(){
        int residuo = 0;
        while (numero > 0){
            residuo = numero % 2;
            pila.apilar(residuo);
            numero = numero / 2;
        }
    }

    public void mostrar() throws PilaVaciaException {
        while (!pila.isEmpty()) System.out.print(pila.desapilar());
    }

}
