/*
Tiempo de ejecución:
Inserción 	Busqueda 	N
2ms 		1ms 		100
1ms 		4ms 		1000
3ms 		4ms 		10000
12ms 		9ms 		100000
61ms 		48ms 		1000000
--------------------
* @autor Eduardo Gómez - 4659580
*/
package ArbolesBinarios;

public class MedicionTiempoBST {

    public static void main(String[] args) {
        TimeEx time = new TimeEx();
        mostrarTitulo();
        for (String element : args){
            int[] nros = generarVector(Integer.parseInt(element));

            //Creacion del arbol de enteros
            BST<Integer> t = new BST<>();
            time.init();
            insertarNros(t,nros);
            time.finish();
            System.out.print("\n"+time.executionTime()+"ms \t\t");

            //Busqueda
            time.init();
            consultarNros(t,nros);
            time.finish();
            System.out.print(time.executionTime()+"ms \t\t");
            System.out.print(element);

            //t.imprimirEnPreOrden();
            //t.imprimirEnOrden();
            //t.imprimirEnPostOrden();
        }
        System.out.println("\n--------------------");

    }

    private static void mostrarTitulo() {
        System.out.println("Tiempo de ejecución:");
        System.out.print("Inserción \t");
        System.out.print("Busqueda \t");
        System.out.print("N \t");
    }

    private static void consultarNros(BST t, int[] nros) {
        for (int k = 0; k < nros.length; k++) t.buscar(nros[k]);
    }

    private static int[] generarVector(int max_elem){ //generacion de valores aleatorios
        int[] nros = new int[max_elem];
        for (int k = 0; k < max_elem; k++) {
            nros[k] = new Integer((int) (Math.random() * 100));
        }
        return nros;
    }

    private static void insertarNros(BST t, int nros[]){
        for (int k = 0; k < nros.length; k++) t.insertar(nros[k]);
    }



}

class TimeEx {
    long tInit, tEnd;

    protected void init(){
        tInit = System.currentTimeMillis();
    }

    public void finish(){
        tEnd = System.currentTimeMillis();
    }

    public long executionTime(){
        return tEnd - tInit;
    }
}