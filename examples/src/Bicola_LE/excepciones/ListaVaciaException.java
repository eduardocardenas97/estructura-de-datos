package Bicola_LE.excepciones;

public class ListaVaciaException extends Throwable{
    public ListaVaciaException() {
        super("Lista Vacía");
    }
}
