package Bicola.excepciones;

public class ColaVaciaException extends Throwable{
    public ColaVaciaException() {
        super("Cola Vacía");
    }
}
