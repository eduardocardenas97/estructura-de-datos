package Bicola.excepciones;

public class ColaLlenaException extends Throwable{
    public ColaLlenaException() {
        super("Cola Llena - Ya no se pueden insertar elementos");
    }
}
