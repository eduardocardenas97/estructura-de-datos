package Bicola;

import Bicola.excepciones.ColaLlenaException;
import Bicola.excepciones.ColaVaciaException;

public class TestBicola {
    public static void main(String[] args) throws ColaLlenaException, ColaVaciaException {
        Bicola cola = new Bicola();
        cola.iniciar(3);
        cola.encolar(new Integer(2));
        cola.encolar(new Integer(32));
        cola.encolar(new Integer(32654));
        cola.mostrarContenido();
        System.out.println("----");
        System.out.println(cola.descolar());
        System.out.println("----");
        System.out.println(cola.descolar());
        System.out.println("----");
        cola.mostrarContenido();
    }
}
