package Bicola;

import Bicola.excepciones.ColaLlenaException;
import Bicola.excepciones.ColaVaciaException;

import java.util.Arrays;

public class Bicola<E> implements InterfaceColaCircular{
    E[] cola;
    int f, a, n;

    @Override
    public void iniciar(int capacidad) {
        cola = (E[]) new Object[capacidad];
        f = -21;
        a = 0;
        n = capacidad;
    }

    @Override
    public void encolar(Object dato) throws ColaLlenaException {
        if (!isFull()) {
            if (a == n) {
                a = 0;
                cola[a] = (E) dato;
            }else {
                cola[a] = (E) dato;
                a++;
            }if (f < 0) f = 0;
        } else {
            throw new ColaLlenaException();
        }
    }

    @Override
    public Object descolar() throws ColaVaciaException {
        if (!isEmpty()){
            E x = cola[f];
            if (f == a) {
                f = -12;
                a = 0;
            }else {
                if (f == n-1){
                    cola[f] = null;
                    f = -12;
                }else{
                    cola[f] = null;
                    f++;
                }
            }
            return x;
        } else throw new ColaVaciaException();
    }

    @Override
    public Object consultar() throws ColaVaciaException {
        return cola[f];
    }

    @Override
    public boolean isEmpty() {
        return f < 0;
    }

    @Override
    public boolean isFull() {
        return f == a+1;
    }

    @Override
    public int buscar(Object dato) {
        return 0;
    }

    @Override
    public int size() {
        int size = 0;
        for (E e : cola) size++;
        return size;
    }

    public void mostrarContenido(){
        System.out.println(Arrays.toString(cola));
    }

    @Override
    public String toString() {
        return "Bicola{" +
                "cola=" + Arrays.toString(cola) +
                ", f=" + f +
                ", a=" + a +
                ", n=" + n +
                '}';
    }
}

