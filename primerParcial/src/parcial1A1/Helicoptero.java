package parcial1A1;
/**
 * @author Eduardo Gomez
 * CI: 4659580
 */
public class Helicoptero extends Transporte{
    private String tipo;

    public Helicoptero(String tipo) {
        this.tipo = tipo;
    }

    public Helicoptero(String matricula, String modelo, int velocidad, String tipo) {
        super(matricula, modelo, velocidad);
        this.tipo = tipo;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Override
    public String toString() {
        return "Helicoptero{" +
                "tipo='" + tipo + '\'' +
                ", matricula='" + matricula + '\'' +
                ", modelo='" + modelo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }

    @Override
    public String despegar() {
        return "El helicoptero despega";
    }

    @Override
    public String volar(float km) {
        return "Vuela: " + km + "km";
    }

    @Override
    public String atrarrizar() {
        return "Aterriza";
    }
}
