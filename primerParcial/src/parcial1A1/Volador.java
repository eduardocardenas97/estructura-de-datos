package parcial1A1;
/**
 * @author Eduardo Gomez
 * CI: 4659580
 */
public interface Volador {
    String despegar();
    String volar(float km);
    String atrarrizar();
}
