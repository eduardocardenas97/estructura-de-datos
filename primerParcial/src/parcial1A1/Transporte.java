package parcial1A1;
/**
 * @author Eduardo Gomez
 * CI: 4659580
 */
public abstract class Transporte implements Volador {
    protected String matricula;
    protected String modelo;
    protected int velocidad;
    
    public Transporte() {
        matricula = "";
        modelo = "";
    }
    
    /*
     Agregue un constructor que reciba todos los parametros	
     y cargue en las propiedades
    */
    public Transporte(String matricula, String modelo, int velocidad) {
        this.matricula = matricula;
        this.modelo = modelo;
        this.velocidad = velocidad;
    }

    //métodos set y get
    public String getMatricula() { return matricula; }
    public int getVelocidad() { return velocidad; }
    public String getModelo() { return modelo; }
    
    public void setMatricula(String matricula) { this.matricula = matricula; }
    public void setVelocidad(int velocidad) { this.velocidad = velocidad; }
    public void setModelo(String modelo) { this.modelo = modelo; }

    /*
     Sobreescriba el método toString heredado de Object
     Para retornar, como una cadena, los datos del transporte
    */
    @Override
    public String toString() {
        return "Transporte{" +
                "matricula='" + matricula + '\'' +
                ", modelo='" + modelo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }
}
