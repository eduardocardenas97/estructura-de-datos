package parcial1A1;
/**
 * @author Eduardo Gomez
 * CI: 4659580
 */
public class Avion extends Transporte{
    private int cantPasajeros;
    private boolean comercial;

    public Avion(int cantPasajeros, boolean comercial) {
        this.cantPasajeros = cantPasajeros;
        this.comercial = comercial;
    }

    public Avion(String matricula, String modelo, int velocidad, int cantPasajeros, boolean comercial) {
        super(matricula, modelo, velocidad);
        this.cantPasajeros = cantPasajeros;
        this.comercial = comercial;
    }

    public int getCantPasajeros() {
        return cantPasajeros;
    }

    public void setCantPasajeros(int cantPasajeros) {
        this.cantPasajeros = cantPasajeros;
    }

    public boolean isComercial() {
        return comercial;
    }

    public void setComercial(boolean comercial) {
        this.comercial = comercial;
    }

    @Override
    public String toString() {
        return "Avion{" +
                "cantPasajeros=" + cantPasajeros +
                ", comercial=" + comercial +
                ", matricula='" + matricula + '\'' +
                ", modelo='" + modelo + '\'' +
                ", velocidad=" + velocidad +
                '}';
    }

    @Override
    public String despegar() {
        return "El avion despega";
    }

    @Override
    public String volar(float km) {
        return "El avion vuela " + km + "km";
    }

    @Override
    public String atrarrizar() {
        return "El avion aterriza";
    }
}
