package parcial1A1;

/**
 * @author Eduardo Gomez
 * CI: 4659580
 */
public class AppTransporte {

    //Declarar una array transpor de Transportes de tamanho 4
    public AppTransporte() {
        //instanciar el array transpor, crear 2 transportes diferentes
        //y cargar en el array
        Transporte[] transpor = new Transporte[4];
        transpor[0] = new Avion("BGR43","Cessna 210",230,5,false);
        transpor[1] = new Helicoptero("B113","Boeing AH-64 Apache",350,"Militar");
        transpor[2] = new Avion("BGG4113","Boeing 777",630,158,true);
        transpor[3] = new Avion("B113","Boeing 767",630,128,true);
        for(int i=0; i<4; i++)
            procesar( transpor[i] );
    }

    public static void main(String[] args) {
        new AppTransporte();
    }

    private void procesar(Transporte t) {
        System.out.println(t.toString());
        System.out.println(t.despegar());
        System.out.println(t.volar(487));
        System.out.println(t.atrarrizar());
        System.out.println();
    }
}
